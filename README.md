# Paper

A pretty note-taking app for Gnome

[![Please do not theme this app](https://stopthemingmy.app/badge.svg)](https://stopthemingmy.app)
[![License: GPL v3](https://img.shields.io/badge/license-GPLv3-orange.svg)](http://www.gnu.org/licenses/gpl-3.0)

## Roadmap
 - Gnome search integration
 - Use Oklab for recoloring
 - Better code blocks

Contributions are appreciated!

## Authors and acknowledgment
 - libadwaita
 - gtksourceview-5

## License
The source code is GPLv3
